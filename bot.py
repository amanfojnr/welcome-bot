#!/usr/bin/python3
import os
import logging
from telegram.ext import (Updater,
                          CommandHandler,
                          MessageHandler,
                          Filters,)

# log activity
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - \
                     %(message)s',
                    level=logging.INFO)

# authorize bot
updater = Updater(os.environ["BOT_TOKEN"])
add_handler = updater.dispatcher.add_handler

# initialize info
welcome_msg = "default"
general_info = "default"
passphrase = os.environ["PASS"]


# commands
def set_general_info(bot, update, args):
    global general_info
    if args[-1] != passphrase:
        update.message.reply_text("Access Denied. Wrong/no passphrase")
    else:
        general_info = " ".join(args[:-1])
        update.message.reply_text("Update successful - {}".format(general_info))


def set_welcome_msg(bot, update, args):
    global welcome_msg
    if args.
    if args[-1] != passphrase:
        update.message.reply_text("Access Denied. Wrong/no passphrase")
    else:
        welcome_msg = " ".join(args[:-1])
        update.message.reply_text("Update successful - {}".format(welcome_msg))


def welcome(bot, update):
    """ welcomes new users """
    update.message.reply_text(welcome_msg)


def show_general_info(bot, update):
    """ 
    listens to conversations for ico related keywords and displays general
    information
    """
    message = update.message.text.lower()
    if "ico" in message or "sale" in message or "about" in message:
        update.message.reply_text(general_info)

# handlers
set_general_info_handler = CommandHandler('info',
                                          set_general_info,
                                          pass_args=True)

set_welcome_msg_handler = CommandHandler('welcome',
                                         set_welcome_msg,
                                         pass_args=True)
show_welcome_msg_handler = MessageHandler(Filters.status_update.new_chat_members,
                                          welcome)
show_general_info_handler = MessageHandler(Filters.text,
                                           show_general_info)

add_handler(set_general_info_handler)
add_handler(set_welcome_msg_handler)
add_handler(show_general_info_handler)
add_handler(show_welcome_msg_handler)


updater.start_polling()
print("Bot connected ...")
updater.idle()
